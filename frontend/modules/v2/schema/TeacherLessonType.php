<?php


namespace frontend\modules\v2\schema;


use GraphQL\Type\Definition\ObjectType;

class TeacherLessonType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'fields' => function () {
                return [
                    'teacher' => [
                        'type' => Types::teacher(),
                    ],
                    'lesson' => [
                        'type' => Types::lesson(),
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }
}
