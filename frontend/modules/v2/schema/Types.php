<?php

namespace frontend\modules\v2\schema;

use frontend\modules\v2\schema\mutations\LessonMutationType;
use frontend\modules\v2\schema\mutations\MutationType;

class Types
{
    private static $query;
    private static $mutation;

    private static $teacher;
    private static $lesson;
    private static $team;
    private static $teacherLesson;
    private static $schedule;

    private static $lessonMutation;

    public static function query()
    {
        return self::$query ?: (self::$query = new QueryType());
    }

    public static function mutation()
    {
        return self::$mutation ?: (self::$mutation = new MutationType());
    }

    public static function teacher()
    {
        return self::$teacher ?: (self::$teacher = new TeacherType());
    }

    public static function lesson()
    {
        return self::$lesson ?: (self::$lesson = new LessonType());
    }

    public static function team()
    {
        return self::$team ?: (self::$team = new TeamType());
    }

    public static function teacherLesson()
    {
        return self::$teacherLesson ?: (self::$teacherLesson = new TeacherLessonType());
    }

    public static function schedule()
    {
        return self::$schedule ?: (self::$schedule = new ScheduleType());
    }

    public static function lessonMutation()
    {
        return self::$lessonMutation ?: (self::$lessonMutation = new LessonMutationType());
    }
}
