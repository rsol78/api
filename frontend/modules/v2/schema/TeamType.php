<?php


namespace frontend\modules\v2\schema;


use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class TeamType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'fields' => function () {
                return [
                    'id' => Type::id(),
                    'name' => Type::string(),
                    'schedules' => [
                        'type' => Type::listOf(Types::schedule())
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }
}
