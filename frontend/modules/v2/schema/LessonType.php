<?php


namespace frontend\modules\v2\schema;


use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class LessonType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'fields' => function () {
                return [
                    'id' => Type::id(),
                    'name' => Type::string(),
                    'schedules' => [
                        'type' => Type::listOf(Types::schedule())
                    ],
                    'teacherLessons' => [
                        'type' => Type::listOf(Types::teacherLesson())
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }
}
