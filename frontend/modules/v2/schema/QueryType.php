<?php

namespace frontend\modules\v2\schema;

use common\models\Lesson;
use common\models\Schedule;
use common\models\Teacher;
use common\models\Team;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use yii\helpers\ArrayHelper;

class QueryType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'fields' => function() {
                return [
                    'lesson' => [
                        'type' => Types::lesson(),
                        'args' => [
                            'id' => Type::nonNull(Type::int()),
                        ],
                        'resolve' => function($root, $args) {
                            return Lesson::findOne($args['id']);
                        }
                    ],
                    'teacher' => [
                        'type' => Types::teacher(),
                        'args' => [
                            'id' => Type::nonNull(Type::int()),
                        ],
                        'resolve' => function($root, $args) {
                            return Teacher::findOne($args['id']);
                        }
                    ],
                    'team' => [
                        'type' => Types::team(),
                        'args' => [
                            'id' => Type::nonNull(Type::int()),
                        ],
                        'resolve' => function($root, $args) {
                            return Team::findOne($args['id']);
                        }
                    ],
                    'lessons' => [
                        'type' => Type::listOf(Types::lesson()),
                        'args' => [
                            'limit' => Type::int(),
                            'offset' => Type::int(),
                        ],
                        'resolve' => function($root, $args) {
                            return Lesson::find()
                                ->limit(ArrayHelper::getValue($args, 'limit', 10))
                                ->offset(ArrayHelper::getValue($args, 'offset', 0))
                                ->all();
                        }
                    ],
                    'teachers' => [
                        'type' => Type::listOf(Types::teacher()),
                        'args' => [
                            'limit' => Type::int(),
                            'offset' => Type::int(),
                        ],
                        'resolve' => function($root, $args) {
                            return Teacher::find()
                                ->limit(ArrayHelper::getValue($args, 'limit', 10))
                                ->offset(ArrayHelper::getValue($args, 'offset', 0))
                                ->all();
                        }
                    ],
                    'teams' => [
                        'type' => Type::listOf(Types::team()),
                        'args' => [
                            'limit' => Type::int(),
                            'offset' => Type::int(),
                        ],
                        'resolve' => function($root, $args) {
                            return Team::find()
                                ->limit(ArrayHelper::getValue($args, 'limit', 10))
                                ->offset(ArrayHelper::getValue($args, 'offset', 0))
                                ->all();
                        }
                    ],
                    'schedule' => [
                        'type' => Type::listOf(Types::schedule()),
                        'args' => [
                            'limit' => Type::int(),
                            'offset' => Type::int(),
                        ],
                        'resolve' => function($root, $args) {
                            return Schedule::find()
                                ->limit(ArrayHelper::getValue($args, 'limit', 10))
                                ->offset(ArrayHelper::getValue($args, 'offset', 0))
                                ->all();
                        }
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }
}
