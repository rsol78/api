<?php


namespace frontend\modules\v2\schema;


use common\models\Schedule;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class ScheduleType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'fields' => function () {
                return [
                    'id' => Type::id(),
                    'day' => Type::int(),
                    'day_name' => [
                        'type' => Type::string(),
                        'resolve' => function (Schedule $model) {
                            return $model->getDayName();
                        }
                    ],
                    'order' => Type::int(),
                    'teacher' => [
                        'type' => Types::teacher(),
                    ],
                    'lesson' => [
                        'type' => Types::lesson(),
                    ],
                    'team' => [
                        'type' => Types::team(),
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }
}
