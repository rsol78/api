<?php


namespace frontend\modules\v2\schema\mutations;


use common\models\Lesson;
use frontend\modules\v2\schema\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class MutationType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'fields' => function () {
                return [
                    'lesson' => [
                        'type' => Types::lessonMutation(),
                        'args' => [
                            'id' => Type::int(),
                        ],
                        'resolve' => function ($root, $args) {
                            return array_key_exists('id', $args)
                                ? Lesson::find()->where($args)->one()
                                : new Lesson();
                        },
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }
}
