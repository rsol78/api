<?php

namespace frontend\modules\v2\schema\mutations;

use common\models\Lesson;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class LessonMutationType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'fields' => function () {
                return [
                    'update' => [
                        'type' => Type::boolean(),
                        'description' => 'Update or create lesson data.',
                        'args' => [
                            'name' => Type::string(),
                        ],
                        'resolve' => function (Lesson $model, $args) {
                            $model->setAttributes($args);
                            return $model->save();
                        }
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }

}
