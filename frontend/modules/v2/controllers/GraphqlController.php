<?php

namespace frontend\modules\v2\controllers;

use frontend\modules\v1\controllers\BaseController;
use frontend\modules\v2\schema\Types;
use GraphQL\GraphQL;
use GraphQL\Type\Schema;
use Yii;
use yii\base\Exception;
use yii\helpers\Json;

/**
 * Default controller for the `v2` module
 */
class GraphqlController extends BaseController
{
    public $modelClass = '';

    /**
     * @inheritdoc
     */
//    protected function verbs()
//    {
//        return [
//            'index' => ['POST'],
//        ];
//    }

    public function actions()
    {
        return [];
    }

    public function actionIndex()
    {
        $query = Yii::$app->request->get('query', Yii::$app->request->post('query'));
        $variables = Yii::$app->request->get('variables', Yii::$app->request->post('variables'));
        $operation = Yii::$app->request->get('operation', Yii::$app->request->post('operation', null));

        if (empty($query)) {
            $rawInput = file_get_contents('php://input');
            $input = json_decode($rawInput, true);
            $query = $input['query'];
            $variables = isset($input['variables']) ? $input['variables'] : [];
            $operation = isset($input['operation']) ? $input['operation'] : null;
        }

        if (!empty($variables) && !is_array($variables)) {
            try {
                $variables = Json::decode($variables);
            } catch (Exception $e) {
                $variables = null;
            }
        }

        $schema = new Schema([
            'query' => Types::query(),
            'mutation' => Types::mutation(),
        ]);

        return GraphQL::executeQuery(
            $schema,
            $query,
            null,
            null,
            empty($variables) ? null : $variables,
            empty($operation) ? null : $operation
        )->toArray(true);
    }
}
