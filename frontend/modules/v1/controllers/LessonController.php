<?php

namespace frontend\modules\v1\controllers;

use common\models\Lesson;

class LessonController extends BaseController
{
    public $modelClass = Lesson::class;
}
