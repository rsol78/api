<?php

namespace frontend\modules\v1\controllers;

use common\models\Schedule;

class ScheduleController extends BaseController
{
    public $modelClass = Schedule::class;
}
