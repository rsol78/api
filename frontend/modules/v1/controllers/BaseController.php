<?php

namespace frontend\modules\v1\controllers;

use common\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\rest\Serializer;

class BaseController extends ActiveController
{
    public $serializer = [
        'class' => Serializer::class,
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = $this->getAuthenticator();
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = $this->getCorsFilter();

        return $behaviors;
    }

    private function getAuthenticator()
    {
        return [
            'class' => CompositeAuth::class,
            'authMethods' => [
                [
                    'class' => HttpBasicAuth::class,
                    'auth' => function ($username, $password) {
                        $model = new LoginForm([
                            'username' => $username,
                            'password' => $password,
                        ]);
                        if ($model->login()) {
                            return $model->getUser();
                        }
                        return null;
                    }
                ],
                [
                    'class' => QueryParamAuth::class,
                    'tokenParam' => 'token'
                ]
            ]
        ];
    }

    private function getCorsFilter()
    {
        $corsOrigin = Yii::$app->params['corsOrigin'];

        $currentServer = parse_url(Yii::$app->request->referrer);
        $scheme = ArrayHelper::getValue($currentServer, 'scheme', 'http');
        $host = ArrayHelper::getValue($currentServer, 'host', $corsOrigin);
        $port = ArrayHelper::getValue($currentServer, 'port');
        $currentServerName = "{$scheme}://{$host}";
        $currentServerName .= $port ? ":{$port}" : '';

        $origin = strpos($currentServerName, $corsOrigin) === false
            ? "http://{$corsOrigin}"
            : $currentServerName;

        return [
            'class' => Cors::class,
            'cors' => [
                'Origin' => [$origin],
                'Access-Control-Request-Headers' => ['X-Wsse'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
    }
}
