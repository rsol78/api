<?php


namespace frontend\modules\v1\controllers;


use common\models\Teacher;

class TeacherController extends BaseController
{
    public $modelClass = Teacher::class;
}
