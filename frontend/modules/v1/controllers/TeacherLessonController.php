<?php

namespace frontend\modules\v1\controllers;

use common\models\TeacherLesson;

class TeacherLessonController extends BaseController
{
    public $modelClass = TeacherLesson::class;
}
