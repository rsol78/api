<?php

namespace frontend\modules\v1\controllers;

use common\models\Team;

class TeamController extends BaseController
{
    public $modelClass = Team::class;
}
