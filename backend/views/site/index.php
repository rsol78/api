<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

$this->registerJs("
$('#submit').on('click', function () {
        $.ajax({
            url: $('#url').val(),
            dataType: 'json',
            success: function (data) {
                $('#response').val(JSON.stringify(data));
            }
        });
    });
");
?>

<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-5">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">URL</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <input type="text" class="form-control" id="url">
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                    </div>
                </div>

            </div>
            <div class="col-lg-7">

                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Response</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <textarea name="" rows="10" class="form-control" id="response"></textarea>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
