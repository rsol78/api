<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%teacher_lesson}}".
 *
 * @property int $id
 * @property int|null $teacher_id
 * @property int|null $lesson_id
 *
 * @property Lesson $lesson
 * @property Teacher $teacher
 */
class TeacherLesson extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%teacher_lesson}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teacher_id', 'lesson_id'], 'integer'],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::class, 'targetAttribute' => ['lesson_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::class, 'targetAttribute' => ['teacher_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacher_id' => 'Teacher ID',
            'lesson_id' => 'Lesson ID',
        ];
    }

    public function fields()
    {
        return [
            'id',
            'teacher_id',
            'teacher' => function() {
                return ArrayHelper::getValue($this->teacher, 'name');
            },
            'lesson_id',
            'lesson' => function() {
                return ArrayHelper::getValue($this->lesson, 'name');
            },
        ];
    }

    /**
     * Gets query for [[Lesson]].
     *
     * @return ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::class, ['id' => 'lesson_id']);
    }

    /**
     * Gets query for [[Teacher]].
     *
     * @return ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::class, ['id' => 'teacher_id']);
    }
}
