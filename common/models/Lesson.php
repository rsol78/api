<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%lesson}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Schedule[] $schedules
 * @property TeacherLesson[] $teacherLessons
 */
class Lesson extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lesson}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function extraFields()
    {
        return ['schedules', 'teacherLessons'];
    }

    /**
     * Gets query for [[Schedules]].
     *
     * @return ActiveQuery
     */
    public function getSchedules()
    {
        return $this->hasMany(Schedule::class, ['lesson_id' => 'id']);
    }

    /**
     * Gets query for [[TeacherLessons]].
     *
     * @return ActiveQuery
     */
    public function getTeacherLessons()
    {
        return $this->hasMany(TeacherLesson::class, ['lesson_id' => 'id']);
    }
}
