<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%teacher}}".
 *
 * @property int $id
 * @property string $name
 * @property int|null $age
 *
 * @property Schedule[] $schedules
 * @property TeacherLesson[] $teacherLessons
 */
class Teacher extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%teacher}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['age'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'age' => 'Age',
        ];
    }

    public function extraFields()
    {
        return ['schedules', 'teacherLessons'];
    }

    /**
     * Gets query for [[Schedules]].
     *
     * @return ActiveQuery
     */
    public function getSchedules()
    {
        return $this->hasMany(Schedule::class, ['teacher_id' => 'id'])->orderBy([
            'day' => SORT_ASC,
            'order' => SORT_ASC,
        ]);
    }

    /**
     * Gets query for [[TeacherLessons]].
     *
     * @return ActiveQuery
     */
    public function getTeacherLessons()
    {
        return $this->hasMany(TeacherLesson::class, ['teacher_id' => 'id']);
    }
}
