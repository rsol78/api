<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%team}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Schedule[] $schedules
 */
class Team extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%team}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function extraFields()
    {
        return ['schedules'];
    }

    /**
     * Gets query for [[Schedules]].
     *
     * @return ActiveQuery
     */
    public function getSchedules()
    {
        return $this->hasMany(Schedule::class, ['team_id' => 'id']);
    }
}
