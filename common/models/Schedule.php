<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%schedule}}".
 *
 * @property int $id
 * @property int|null $day
 * @property int|null $order
 * @property int|null $teacher_id
 * @property int|null $lesson_id
 * @property int|null $team_id
 *
 * @property Lesson $lesson
 * @property Teacher $teacher
 * @property Team $team
 */
class Schedule extends ActiveRecord
{
    const DAY_MON = 1;
    const DAY_TUE = 2;
    const DAY_WED = 3;
    const DAY_THU = 4;
    const DAY_FRI = 5;
    const DAY_SAT = 6;
    const DAY_SUN = 7;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%schedule}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['day', 'order', 'teacher_id', 'lesson_id', 'team_id'], 'integer'],
            [['day'], 'in', 'range' => array_keys(self::dayList())],
            [['order'], 'in', 'range' => range(1, 12)],
            [
                ['lesson_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Lesson::class,
                'targetAttribute' => ['lesson_id' => 'id']
            ],
            [
                ['teacher_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Teacher::class,
                'targetAttribute' => ['teacher_id' => 'id']
            ],
            [
                ['team_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Team::class,
                'targetAttribute' => ['team_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'day' => 'Day',
            'order' => 'Order',
            'teacher_id' => 'Teacher ID',
            'lesson_id' => 'Lesson ID',
            'team_id' => 'Team ID',
        ];
    }

    public static function dayList()
    {
        return [
            self::DAY_MON => 'Monday',
            self::DAY_TUE => 'Tuesday',
            self::DAY_WED => 'Wednesday',
            self::DAY_THU => 'Thursday',
            self::DAY_FRI => 'Friday',
            self::DAY_SAT => 'Saturday',
            self::DAY_SUN => 'Sunday',
        ];
    }

    public function getDayName()
    {
        return ArrayHelper::getValue(self::dayList(), $this->day, '-');
    }

    public function fields()
    {
        return [
            'id',
            'day',
            'day_name' => function() {
                return $this->getDayName();
            },
            'order',
            'teacher_id',
            'teacher' => function() {
                return ArrayHelper::getValue($this->teacher, 'name');
            },
            'lesson_id',
            'lesson' => function() {
                return ArrayHelper::getValue($this->lesson, 'name');
            },
            'team_id',
            'team' => function() {
                return ArrayHelper::getValue($this->team, 'name');
            },
        ];
    }

    /**
     * Gets query for [[Lesson]].
     *
     * @return ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::class, ['id' => 'lesson_id']);
    }

    /**
     * Gets query for [[Teacher]].
     *
     * @return ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::class, ['id' => 'teacher_id']);
    }

    /**
     * Gets query for [[Team]].
     *
     * @return ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::class, ['id' => 'team_id']);
    }
}
