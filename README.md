Yii2 REST & GRAPHQL
==========

## Common AUTH

Two methods:

- Base auth: admin/qwerty
- By "token" GET param (see in your DB)

## CORS

_`USE CORS SETTINGS IN YII2 - NOT GOOD IDEA. SHOULD BE MOVED TO SERVER SETTINGS`_

See
`app/frontend/config/params.php:4`

`php
'corsOrigin' => 'example.site',
`

Tested from Backend `site/index`

## V1 - REST
`
GET /v1/teachers?expand=schedules&fields=name,schedules.day_name,schedules.team,schedules.order
`
and other

## V2 - GRAPHQL

#### Receive data

`
/v2/graphqls?query={schedule(limit:10){id,day_name,order,teacher{name},lesson{name},team{name}}}
`
and other

#### Update data

`
/v2/graphqls?query=mutation{lesson(id:1){update(name:"Math")}}
`

#### Create data

``
/v2/graphqls?query=mutation{lesson{update(name:"Calc")}}
``
