<?php

use Faker\Generator;
use yii\db\Migration;

/**
 * Class m200331_085208_fill
 */
class m200331_085208_fill extends Migration
{
    /**
     * @var Generator
     */
    private $faker;

    public function init()
    {
        $this->faker = Faker\Factory::create();
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lesson();
        $this->teacher();
        $this->team();

        $this->teacher_lesson();
        $this->schedule();
    }

    private function schedule()
    {
        $data = [];
        foreach (range(1, 100) as $item) {
            $data[] = [
                'day' => $this->faker->numberBetween(1, 7),
                'order' => $this->faker->numberBetween(1, 7),
                'teacher_id' => $this->faker->numberBetween(1, 5),
                'lesson_id' => $this->faker->numberBetween(1, 5),
                'team_id' => $this->faker->numberBetween(1, 5),
            ];
        }
        $this->batchInsert('schedule', ['day', 'order', 'teacher_id', 'lesson_id', 'team_id'], $data);
    }

    private function teacher_lesson()
    {
        $data = [];
        foreach (range(1, 10) as $item) {
            $data[] = [
                'teacher_id' => $this->faker->numberBetween(1, 5),
                'lesson_id' => $this->faker->numberBetween(1, 5),
            ];
        }
        $this->batchInsert('teacher_lesson', ['teacher_id', 'lesson_id'], $data);
    }

    private function team()
    {
        $data = [];
        foreach (range(1, 5) as $item) {
            $data[] = [
                'name' => $this->faker->numberBetween(4, 11) . $this->faker->randomLetter,
            ];
        }
        $this->batchInsert('team', ['name'], $data);
    }

    private function teacher()
    {
        $data = [];
        foreach (range(1, 5) as $item) {
            $data[] = [
                'name' => $this->faker->name,
                'age' => $this->faker->numberBetween(22, 60),
            ];
        }
        $this->batchInsert('teacher', ['name', 'age'], $data);
    }

    private function lesson()
    {
        $data = [
            [
                'name' => 'Math',
            ],
            [
                'name' => 'Lit',
            ],
            [
                'name' => 'Music',
            ],
            [
                'name' => 'Sport',
            ],
            [
                'name' => 'English',
            ],
        ];
        $this->batchInsert('lesson', ['name'], $data);
    }

    public function down()
    {
        $this->delete('team');
        $this->delete('teacher');
        $this->delete('lesson');

        $this->truncateTable('schedule');
        $this->truncateTable('teacher_lesson');
        $this->truncateTable('team');
        $this->truncateTable('teacher');
        $this->truncateTable('lesson');
    }
}
