<?php

use yii\db\Migration;

/**
 * Class m200331_085144_schedule
 */
class m200331_085144_schedule extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('schedule', [
            'id' => $this->primaryKey(),
            'day' => $this->smallInteger()->unsigned(),
            'order' => $this->smallInteger()->unsigned(),
            'teacher_id' => $this->integer(),
            'lesson_id' => $this->integer(),
            'team_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_schedule_teacher_id', 'schedule', 'teacher_id', 'teacher', 'id', 'CASCADE');
        $this->addForeignKey('fk_schedule_lesson_id', 'schedule', 'lesson_id', 'lesson', 'id', 'CASCADE');
        $this->addForeignKey('fk_schedule_team_id', 'schedule', 'team_id', 'team', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_schedule_teacher_id', 'schedule');
        $this->dropForeignKey('fk_schedule_lesson_id', 'schedule');
        $this->dropForeignKey('fk_schedule_team_id', 'schedule');

        $this->dropTable('schedule');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200331_085144_schedule cannot be reverted.\n";

        return false;
    }
    */
}
