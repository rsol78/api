<?php

use common\models\User;
use frontend\models\SignupForm;
use yii\db\Migration;

/**
 * Class m200331_114157_new_user
 */
class m200331_114157_new_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $user = new User();
        $user->username = 'admin';
        $user->email = 'admin@admin.com';
        $user->setPassword('qwerty');
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->status = User::STATUS_ACTIVE;
        return $user->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if ($user = User::findByUsername('admin')) {
            $user->delete();
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200331_114157_new_user cannot be reverted.\n";

        return false;
    }
    */
}
