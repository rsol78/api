<?php

use yii\db\Migration;

/**
 * Class m200331_085120_teacher_lesson
 */
class m200331_085120_teacher_lesson extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('teacher_lesson', [
            'id' => $this->primaryKey(),
            'teacher_id' => $this->integer(),
            'lesson_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_teacher_lesson_teacher_id', 'teacher_lesson', 'teacher_id', 'teacher', 'id', 'CASCADE');
        $this->addForeignKey('fk_teacher_lesson_lesson_id', 'teacher_lesson', 'lesson_id', 'lesson', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_teacher_lesson_teacher_id', 'teacher_lesson');
        $this->dropForeignKey('fk_teacher_lesson_lesson_id', 'teacher_lesson');

        $this->dropTable('teacher_lesson');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200331_085120_teacher_lesson cannot be reverted.\n";

        return false;
    }
    */
}
